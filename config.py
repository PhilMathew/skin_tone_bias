from pathlib import Path

IMAGE_SIZE = (640, 480)
TRAIN_CSV = Path('/home/mathepa1/sd-198/sd_198_train.csv')
TEST_CSV = Path('/home/mathepa1/sd-198/sd_198_test.csv')
VAL_CSV = Path('/home/mathepa1/sd-198/sd_198_val.csv')
SKIN_TONE_CSV = Path('/home/mathepa1/Projects/skin_tone_paper_replication/sd_198_results/skin_tones.csv')
METRICS_DIR = Path.cwd() / 'model_evals'
CLF_MODEL_PATH = Path('/home/mathepa1/Projects/skin_tone_paper_replication/disease_classification/clf_train_runs/disease_classifier_densenet_PID22485/weights.hdf5')