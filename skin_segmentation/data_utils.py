import random

import numpy as np
import cv2
import keras


def randomly_flip_image(image, mask):
    x = random.randint(1, 10)
    if x <= 5:
        return cv2.flip(image, 1), cv2.flip(mask, 1)
    else:
        return image, mask


class Dataset:
    def __init__(self, im_paths, mask_root_dir, image_size=(600, 450), augmentations=None, preprocessing=None):
        self.im_paths = im_paths
        self.mask_paths = []
        if mask_root_dir is not None:
            for path in im_paths:
                self.mask_paths.append(list(mask_root_dir.glob(path.stem + '*'))[0])

        self.im_w, self.im_h = image_size
        self.aug_funcs = augmentations
        self.preproc_func = preprocessing

    def __getitem__(self, item_idx):
        img = cv2.cvtColor(cv2.imread(str(self.im_paths[item_idx])), cv2.COLOR_BGR2RGB)
        img = cv2.resize(img, (self.im_w, self.im_h))
        if len(self.mask_paths) != 0:
            mask = self._load_mask(mask_path=self.mask_paths[item_idx])
        else:
            mask = np.zeros(shape=(img.shape[0], img.shape[1], 2))

        if self.preproc_func is not None:
            img = self.preproc_func(img)

        if self.aug_funcs is not None:
            for f in self.aug_funcs:
                img, mask = f(img, mask)

        return img, mask

    def __len__(self):
        return len(self.im_paths)

    def _load_mask(self, mask_path):
        mask = cv2.cvtColor(cv2.imread(str(mask_path)), cv2.COLOR_BGR2GRAY)
        mask = cv2.resize(mask, (self.im_w, self.im_h))
        mask = mask // 255
        mask = np.expand_dims(mask, axis=-1)
        mask = keras.utils.to_categorical(mask)

        return mask


class DataLoader(keras.utils.Sequence):
    def __init__(self, dataset, batch_size=32, shuffle_every_epoch=False, return_masks=True):
        self.dataset = dataset
        self.batch_size = batch_size if batch_size <= len(dataset) else len(dataset)
        self.shuffle_every_epoch = shuffle_every_epoch
        self.return_masks = return_masks
        self.indices = np.arange(len(dataset))

        self.on_epoch_end()

    def __getitem__(self, item_idx):
        start, stop = item_idx * self.batch_size, (item_idx + 1) * self.batch_size
        if stop > len(self.dataset):
            stop = len(self.dataset)

        batch_imgs, batch_masks = [], []
        for i in range(start, stop):
            img, mask = self.dataset[i]
            batch_imgs.append(img)
            batch_masks.append(mask)

        if self.return_masks:
            return np.asarray(batch_imgs), np.asarray(batch_masks)
        else:
            return np.asarray(batch_imgs)

    def __len__(self):
        return (len(self.dataset) // self.batch_size) + (1 if len(self.dataset) % self.batch_size > 0 else 0)

    def on_epoch_end(self):
        if self.shuffle_every_epoch:
            self.indices = np.random.permutation(self.indices)