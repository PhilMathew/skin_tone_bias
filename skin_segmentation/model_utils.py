import multiprocessing

import cv2
import keras
import numpy as np
import segmentation_models as sm

from skin_segmentation.segmentation_cfg import *
from skin_segmentation.data_utils import DataLoader, Dataset


def create_model(backbone, num_classes=2, lr=.001):
    callbacks = [keras.callbacks.ModelCheckpoint(str(MODEL_PATH), save_best_only=True, mode='min')]
    model = sm.Unet(backbone, classes=num_classes, activation='softmax')
    optim = keras.optimizers.Adam(lr)
    metrics = [sm.metrics.IOUScore(threshold=0.5), sm.metrics.FScore(threshold=0.5)]
    model.compile(optim, sm.losses.binary_focal_loss, metrics)

    return model, callbacks, metrics


def train_model(train_set, val_set, batch_size, backbone, epochs, lr):
    train_loader = DataLoader(train_set, batch_size=batch_size)
    val_loader = DataLoader(val_set, batch_size=batch_size)

    model, callbacks, _ = create_model(backbone, lr=lr)

    if RETURN_SUMMARY:
        model.summary()

    if not OUTPUT_DIR.exists():
        OUTPUT_DIR.mkdir()

    hist = model.fit_generator(train_loader,
                               steps_per_epoch=len(train_loader),
                               epochs=epochs,
                               callbacks=callbacks,
                               validation_data=val_loader,
                               validation_steps=len(val_loader),
                               use_multiprocessing=True,
                               workers=multiprocessing.cpu_count())

    return hist


def predict_mask(img, model_path):
    model = keras.models.load_model(str(model_path),
                                    custom_objects={'binary_focal_loss': sm.losses.binary_focal_loss,
                                                    'iou_score': sm.metrics.IOUScore(threshold=0.5),
                                                    'f1-score': sm.metrics.FScore(threshold=0.5)})
    model_input = img

    if len(model_input.shape) == 4:
        pred = model.predict(model_input)
    else:
        pred = model.predict(np.expand_dims(model_input, axis=0))

    return model_input, make_mask_usable(pred)


def predict_mask_generator(img_paths, model_path, image_size, batch_size):
    img_loader = DataLoader(dataset=Dataset(img_paths, mask_root_dir=None, image_size=image_size),
                            batch_size=batch_size, return_masks=False)

    model = keras.models.load_model(str(model_path),
                                    custom_objects={'binary_focal_loss': sm.losses.binary_focal_loss,
                                                    'iou_score': sm.metrics.IOUScore(threshold=0.5),
                                                    'f1-score': sm.metrics.FScore(threshold=0.5)})

    preds = model.predict_generator(img_loader, use_multiprocessing=True, workers=multiprocessing.cpu_count(), verbose=1)

    return img_loader, make_mask_usable(preds)


def test_model(test_set, model_path, batch_size):
    test_loader = DataLoader(test_set, batch_size=batch_size)

    model = keras.models.load_model(str(model_path),
                                    custom_objects={'binary_focal_loss': sm.losses.binary_focal_loss,
                                                    'iou_score': sm.metrics.IOUScore(threshold=0.5),
                                                    'f1-score': sm.metrics.FScore(threshold=0.5)})

    scores = model.evaluate_generator(test_loader, use_multiprocessing=True, workers=multiprocessing.cpu_count())
    with open(str(OUTPUT_DIR / "test_metrics.txt"), 'a+') as f:
        for metric, val in zip(['IOU Score', 'F1 Score'], scores[1:]):
            f.write(f"Mean {metric}: {val}\n")


def make_mask_usable(model_pred):
    mask = np.argmax(model_pred, axis=-1)

    if mask.shape[0] == 1:
        mask = np.squeeze(mask, axis=0)

    mask = np.expand_dims(mask, axis=-1)

    return mask