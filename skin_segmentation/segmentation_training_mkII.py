from sklearn.utils import shuffle

from skin_segmentation.segmentation_cfg import *
from config import *
from skin_segmentation.data_utils import Dataset, randomly_flip_image
from skin_segmentation.model_utils import train_model, test_model
from skin_segmentation.plot_utils import plot_images_w_masks, plot_history


if IMAGE_SIZE[0] % 32 != 0 or IMAGE_SIZE[1] % 32 != 0: # ensures dimensions are usable
    aspect_ratio = float(max(IMAGE_SIZE)) / min(IMAGE_SIZE)
    new_min = ((min(IMAGE_SIZE) // 32) + 1) * 32
    new_max = new_min * aspect_ratio
    new_dims = (int(new_min), int(new_max)) if IMAGE_SIZE[0] == min(IMAGE_SIZE) else (int(new_max), int(new_min))

    raise ValueError(f"Image size must be a multiple of 32 (suggested size: {new_dims[0]}x{new_dims[1]})")


def create_train_val_and_test_sets(img_root_dir, mask_root_dir, train_size):
    im_paths = list(img_root_dir.glob('*.jpg'))
    im_paths = shuffle(im_paths, random_state=RANDOM_STATE)
    train_ims, test_val_ims = im_paths[0:int(len(im_paths) * train_size)], im_paths[int(len(im_paths) * train_size):]
    val_ims, test_ims = test_val_ims[:len(test_val_ims)//2], test_val_ims[len(test_val_ims)//2:]

    train_set = Dataset(im_paths=train_ims, image_size=IMAGE_SIZE, mask_root_dir=mask_root_dir, augmentations=[randomly_flip_image])
    val_set = Dataset(im_paths=val_ims, image_size=IMAGE_SIZE, mask_root_dir=mask_root_dir, augmentations=None)
    test_set = Dataset(im_paths=test_ims, image_size=IMAGE_SIZE, mask_root_dir=mask_root_dir, augmentations=None)

    return train_set, val_set, test_set


def main():
    train_set, val_set, test_set = create_train_val_and_test_sets(IMG_ROOT_DIR, MASK_ROOT_DIR, TRAIN_SIZE)
    # hist = train_model(train_set, val_set, BATCH_SIZE, BACKBONE, EPOCHS, LR)
    # plot_history(hist)
    test_model(test_set, MODEL_PATH, BATCH_SIZE)
    plot_images_w_masks(test_set, num_plots=10, save_plots=True)


if __name__=='__main__':
    main()

