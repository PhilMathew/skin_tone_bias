import numpy as np
import matplotlib.pyplot as plt
from segmentation_models.metrics import IOUScore
from tqdm import trange
import keras

from skin_segmentation.segmentation_cfg import *
from skin_segmentation.model_utils import predict_mask

def plot_images_w_masks(dataset, num_plots='all', save_plots=False, show_plots=False):
    if num_plots is 'all':
        num = len(dataset)
    else:
        num = num_plots

    for i in trange(num):
        img, gt_mask = dataset[i][0], np.argmax(dataset[i][1], axis=-1)
        _, pr_mask = predict_mask(img, model_path=MODEL_PATH)
        masked_im = img * pr_mask
        iou = IOUScore(threshold=0.5)(keras.utils.to_categorical(gt_mask), keras.utils.to_categorical(pr_mask))

        items_to_plot = {"Image": img, "Mask": np.squeeze(pr_mask), "Combined": masked_im}

        fig, im_axes = plt.subplots(nrows=1, ncols=3)
        fig.suptitle(f"IOU Score: {iou}")
        for plot_idx, item_name in enumerate(items_to_plot):
            im_axes[plot_idx].imshow(items_to_plot[item_name])
            im_axes[plot_idx].set_title(item_name)

        if save_plots:
            save_dir = OUTPUT_DIR / 'predictions'
            if not save_dir.exists():
                save_dir.mkdir()

            fig.savefig(str(save_dir / f'output{i}.png'))

        if show_plots:
            plt.show()

def plot_history(hist):
    hist_metrics = hist.history

    for metric in hist_metrics.keys():
        fig, ax = plt.subplots(1, 1)
        ax.plot(hist_metrics[metric])
        ax.set(xlabel='Epoch', ylabel=metric)
        ax.grid(True)
        fig.savefig(str(OUTPUT_DIR / f'{metric}.png'))
