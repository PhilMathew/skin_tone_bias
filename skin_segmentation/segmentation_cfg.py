from pathlib import Path

RANDOM_STATE = 2020
IMG_ROOT_DIR = Path("/home/mathepa1/sd-198/isic2018/ISIC2018_Task1-2_Training_Input")
MASK_ROOT_DIR = Path("/home/mathepa1/skin_lesion_paper/isic2018/ISIC2018_Task1_Training_GroundTruth")
OUTPUT_DIR = Path.cwd() / "segmentation_output"
MODEL_PATH = OUTPUT_DIR / 'best_model.h5'
TRAIN_SIZE = .9
BACKBONE = 'resnet50'
RETURN_SUMMARY = False
BATCH_SIZE = 8 # 32
LR = 0.0001
EPOCHS = 25