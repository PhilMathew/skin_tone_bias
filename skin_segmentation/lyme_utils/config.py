from pathlib import Path

IMG_ROOT_DIR = Path('/home/mathepa1/lyme_data/images_July2019')
IMAGE_CSV = Path('/home/mathepa1/Projects/skin_tone_paper_replication/skin_segmentation/lyme_utils/NO_vs_EM_vs_HZ_vs_TC_vs_IB_vs_IB-T_vs_CELL_vs_EMU_total.csv')
LABELS_JSON = Path('/home/mathepa1/Projects/skin_tone_paper_replication/skin_segmentation/lyme_utils/lyme_template_masks.json')