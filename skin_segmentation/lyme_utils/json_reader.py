import json

import cv2
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt

from skin_segmentation.lyme_utils.config import *


def collect_coordinates(polygon_dict, im_shape):
    coords = []
    for raw_coord in polygon_dict:
        coord = []
        for j, v in enumerate(reversed(list(raw_coord.values()))):
            coord.append(round(v) if round(v) < im_shape[j] else im_shape[j] - 1)
        coords.append(list(reversed(coord)))

    return coords


def clean_mask_data(mask_data):
    obj_labels, obj_counts = np.unique([mask_obj['title'] for mask_obj in mask_data], return_counts=True)
    mask_dict = {k: [mask_obj for mask_obj in mask_data if mask_obj['title'] == k] for k in obj_labels}

    for i, label in enumerate(obj_labels):  # ensures that the gt_mask data was categorized correctly
        assert len(mask_dict[label]) == obj_counts[i]

    return mask_dict


def generate_masks(raw_mask_data, im_shape):
    mask_data = clean_mask_data(raw_mask_data)

    mask_dict = dict.fromkeys(mask_data.keys())
    for label, data in zip(mask_data.keys(), mask_data.values()):
        curr_mask = np.zeros(shape=im_shape)

        for i, mask_obj in enumerate(data):
            coords = collect_coordinates(mask_obj['polygon'], curr_mask.shape)
            cv2.fillPoly(curr_mask, np.array([coords], dtype=np.int32), color=(255, 255, 255))

        mask_dict[label] = cv2.cvtColor(np.uint8(curr_mask), cv2.COLOR_RGB2GRAY) // 255

    if len(mask_dict.keys()) > 1:
        mask_dict['Skin'] = mask_dict['Skin'] - mask_dict['Lesion']  # ensures that the lesion is subtracted from the skin gt_mask
        mask_dict['Skin'][mask_dict['Skin'] != 1] = 0  # prevents overflow errors

    return mask_dict


def mask_image(im_path, mask_data):
    p = IMG_ROOT_DIR / im_path
    if p.is_file():
        im = cv2.cvtColor(cv2.imread(str(IMG_ROOT_DIR / im_path)), cv2.COLOR_BGR2RGB)
    else:
        raise FileNotFoundError(f'{str(p)} is not a valid image')

    masks = generate_masks(mask_data, im.shape)
    msk_ims = [im * np.expand_dims(mask, axis=-1) for mask in masks.values()]

    return msk_ims


def main():
    img_csv = pd.read_csv(str(IMAGE_CSV))
    with open(str(LABELS_JSON)) as f:
        json_list = json.load(f)

    for im_data in json_list:
        path_results = [p for p in img_csv['image'] if im_data['External ID'] in p]
        if len(path_results) > 0:
            im_path = path_results[0]  # no duplicates in our csv, so there should only be one result (at index 0)
            mask_data = im_data['Label']['objects']
        else:
            print(f"{im_data['External ID']} not found in image csv, skipping it for now")
            continue

        msk_ims = mask_image(im_path, mask_data)
        fig, axes = plt.subplots(nrows=1, ncols=2)

        for i, im in enumerate(msk_ims):
            axes[i].imshow(im)
        plt.show()
        plt.close()


if __name__ == '__main__':
    main()
