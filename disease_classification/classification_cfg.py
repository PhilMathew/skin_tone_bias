from pathlib import Path

RANDOM_STATE = 2020
IMG_ROOT_DIR = Path("/home/mathepa1/skin_lesion_paper/isic2018/ISIC2018_Task3_Training_Input")
SEGMENTATION_MODEL_PATH = Path('/home/mathepa1/Projects/skin_tone_paper_replication/skin_segmentation/segmentation_output/best_model.h5')
SKIN_TONE_CSV = Path('/home/mathepa1/Projects/skin_tone_paper_replication/disease_classification/skin_tones.csv')
LABELS_CSV = Path('/home/mathepa1/skin_lesion_paper/isic2018/ISIC2018_Task3_Training_GroundTruth/ISIC2018_Task3_Training_GroundTruth.csv')
BATCH_SIZE = 512