import time
import math

import cv2
from tqdm import tqdm
import numpy as np
import pandas as pd

from skin_segmentation.model_utils import predict_mask_generator
from disease_classification.classification_cfg import *
from config import *


def calc_ITA(img_paths, batch_size):
    img_loader, masks = predict_mask_generator(img_paths, SEGMENTATION_MODEL_PATH, IMAGE_SIZE, batch_size=batch_size)

    ita_vals = []
    for i, imgs in enumerate(tqdm(img_loader, desc="Computing mean ITA for each image in batch")):
        # using np.hstack() on the images allows us to perform cv2 operations on the whole batch at once
        img_stack = np.hstack(imgs)

        lab_blurred_stack = cv2.cvtColor(cv2.medianBlur(img_stack.astype(np.uint8), ksize=13),
                                         cv2.COLOR_RGB2LAB)  # blurs images and converts them to CIELab colorspace
        ita_stack = np.arctan((lab_blurred_stack[:, :, 0] - 50) / lab_blurred_stack[:, :, 2]) * \
                    (180 / math.pi)  # ITA formula as paper defines it

        # We iterate through each ITA map and apply the corresponding gt_mask to it,
        # thus removing any pixels which are part of the lesion.
        ita_vals.extend([np.mean(ita_arr[masks[(i * batch_size) + j].squeeze() == 0])
                         for j, ita_arr in enumerate(np.hsplit(ita_stack, indices_or_sections=len(imgs)))])

    return ita_vals


def main():
    img_paths = [str(path) for path in IMG_ROOT_DIR.glob('*.jpg') if path.is_file()] # grabs a list of the image paths
    df = pd.DataFrame(columns=['image', 'category', 'raw_ita']) # creates the dataframe
    # skin_tone_df = pd.read_csv(SKIN_TONE_CSV)

    # Predicting all masks at once is too memory intensive, so we do it in batches
    s = time.time()
    pbar = tqdm(range(0, len(img_paths), BATCH_SIZE), desc="Categorizing images")
    for i in pbar:
        batch = img_paths[i:i + BATCH_SIZE]
        ita_vals = calc_ITA(batch, batch_size=16) # use smaller batch size in prediction just to be safe

        # Saves the skin tone category for each image to the dataframe (categories defined in
        # skin_tone_categorization_scheme.txt, which is pulled from Table 1 in the original paper).
        for path, ita in zip(batch, ita_vals):
            if ita > 55:  # Yeah this is disgusting to lay eyes on, idk how to shorten it
                tone = 'very_lt'
            elif 48 < ita <= 55:
                tone = 'lt2'
            elif 41 < ita <= 58:
                tone = 'lt1'
            elif 34.5 < ita <= 41:
                tone = 'int2'
            elif 28 < ita <= 34.5:
                tone = 'int1'
            elif 19 < ita <= 28:
                tone = 'tan2'
            elif 10 < ita <= 19:
                tone = 'tan1'
            else:
                tone = 'dark'

            df = df.append({'image':str(Path(path).name), 'category':tone, 'raw_ita':ita}, ignore_index=True) # Apparently skin_tone_df.append() doesn't append in place...y tho
        df.to_csv(str(SKIN_TONE_CSV), columns=df.columns, index=False) # save each iteration in case errors occur later
        pbar.update()
    print(f"Operation took {time.time() - s} seconds for {len(img_paths)} images")


if __name__ == '__main__':
    main()