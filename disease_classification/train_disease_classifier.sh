#!/usr/bin/env bash

cd ..
/home/mathepa1/.conda/envs/semantic_segmentation/bin/train_model \
--root_im_dir='/home/mathepa1/skin_lesion_paper/isic2018/ISIC2018_Task3_Training_Input_Resized' \
--train_csv='/home/mathepa1/skin_lesion_paper/isic2018/ISIC2018_Task3_Training_Input_Resized/train.csv' \
--val_csv='/home/mathepa1/skin_lesion_paper/isic2018/ISIC2018_Task3_Training_Input_Resized/val.csv' \
--test_csv='/home/mathepa1/skin_lesion_paper/isic2018/ISIC2018_Task3_Training_Input_Resized/test.csv' \
--csv_file_path_column_name='image' \
--csv_target_column_name='label' \
--root_run_dir='/home/mathepa1/Projects/skin_tone_paper_replication/disease_classification/clf_train_runs' \
--run_desc='disease_classifier_densenet' \
--model='DenseNet201' \
--cuda_devices='0' \
--use_multiprocessing \
--num_epochs='300' \
--early_stopping_patience='100' \
--optimizer='Adam' \
--data_aug