from tqdm import trange
import cv2
import pandas as pd
import numpy as np

from disease_classification.classification_cfg import *
from config import *

orig_csv = pd.read_csv(str(LABELS_CSV))
new_dir = Path('/home/mathepa1/skin_lesion_paper/isic2018/ISIC2018_Task3_Training_Input_Resized')

new_dict = {'image': [], 'label': []}
for i in trange(len(orig_csv.index)):
    curr_row = orig_csv.iloc[i].to_dict()
    new_path = str(new_dir / (curr_row['image'] + '.jpg'))
    new_dict['image'].append(curr_row['image'] + '.jpg')
    cv2.imwrite(new_path, cv2.resize(cv2.imread(str(IMG_ROOT_DIR  / (curr_row['image'] + '.jpg'))), IMAGE_SIZE))

    labels_dict = {'MEL': 0, 'NV': 1, 'BCC': 2, 'AKIEC': 3, 'BKL': 4, 'DF': 5, 'VASC': 6}
    for key, val in zip(curr_row.keys(), curr_row.values()):
        if val == 1:
            new_dict['label'].append(labels_dict[key])
            break

new_csv = pd.DataFrame(new_dict)

train_msk = np.random.rand(len(new_csv)) < .8
train_csv = new_csv[train_msk]
testval = new_csv[~train_msk]

val_msk = np.random.rand(len(testval)) < .5
val_csv = testval[val_msk]
test_csv = testval[~val_msk]

train_csv.to_csv(str(new_dir / 'train.csv'), columns=['image', 'label'], index=False)
val_csv.to_csv(str(new_dir / 'val.csv'), columns=['image', 'label'], index=False)
test_csv.to_csv(str(new_dir / 'test.csv'), columns=['image', 'label'], index=False)
