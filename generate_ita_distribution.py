from tqdm import tqdm
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt

from config import *


def create_bar_chart(save_path, skin_tone_csv, dataset_csv=None, title=None):
    skin_tone_df = pd.read_csv(str(skin_tone_csv))

    if dataset_csv is not None:
        data_df = pd.read_csv(str(dataset_csv))

        df_to_use = pd.DataFrame(columns=skin_tone_df.columns)
        for i, img in enumerate(tqdm(skin_tone_df['image'])):
            if img in list(data_df['image']):
                df_to_use = df_to_use.append(skin_tone_df.iloc[i], ignore_index=True)
        df_to_use = df_to_use.reset_index()
    else:
        df_to_use = skin_tone_df

    labels = ['very_lt', 'lt2', 'lt1', 'int2', 'int1', 'tan2', 'tan1', 'dark']
    vals, counts = np.unique(df_to_use['category'], return_counts=True)  # bincount doesn't work for some reason
    counts = [counts[list(vals).index(i)] if i in vals else 0 for i in labels]

    fig, ax = plt.subplots(nrows=1, ncols=1)

    ax.bar(labels, counts)
    for i, v in enumerate(counts):
        v_offset = max(counts) // 100
        ax.text(i, v + v_offset, str(v), color='black', fontweight='bold', ha='center')

    if title is not None:
        ax.set_title(title)

    fig.savefig(save_path)
    plt.close()


def main():
    csvs = [TRAIN_CSV, TEST_CSV, VAL_CSV]

    create_bar_chart('full_dataset_dist.png', SKIN_TONE_CSV)
    for csv in tqdm(csvs):
        create_bar_chart(f'{csv.stem}_dataset_dist.png', SKIN_TONE_CSV, csv, title=csv.stem.capitalize())


if __name__=='__main__':
    main()