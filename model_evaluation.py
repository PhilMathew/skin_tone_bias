import json
import multiprocessing

import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from keras.models import load_model
from dlk.data import ImageBatchSequence
from dlk.evaluation.metrics import evaluate_performance_metrics

from disease_classification.classification_cfg import *
from disease_classification.train_clf import plot_confmat
from config import *


def separate_by_ita(skin_tone_csv_path, test_csv_path):
    skin_tone_df = pd.read_csv(str(skin_tone_csv_path))
    test_df = pd.read_csv(str(test_csv_path))
    skin_tone_dict = {key:[] for key in np.unique(skin_tone_df['category'])}
    for key in skin_tone_dict.keys():
        skin_tone_dict[key] = [im_path for im_path in skin_tone_df[skin_tone_df['category'] == key]['image']
                               if im_path in list(test_df['image'])]

    return skin_tone_dict


def evaluate_on_categories(im_paths, gt_labels, skin_tone_dict, metrics_dir):
    model = load_model(str(CLF_MODEL_PATH))
    seq = ImageBatchSequence(im_paths, y=np.zeros(len(im_paths)), batch_size=32, root_im_dir=IMG_ROOT_DIR,
                             target_size=(224, 224, 3), do_imagenet_preprocess=True, shuffle_every_epoch=False,
                             return_y=False, load_all_data_now=False)
    pr_labels = model.predict_generator(seq, use_multiprocessing=True, workers=multiprocessing.cpu_count(), verbose=1)

    for cat_name, cat_ims in zip(skin_tone_dict.keys(), skin_tone_dict.values()):
        indices = [list(im_paths).index(path) for path in cat_ims]
        cat_pr_labels = np.asarray([pr_labels[i] for i in indices])
        cat_gt_labels = np.asarray([gt_labels[i] for i in indices])

        save_dir = metrics_dir / cat_name
        if not save_dir.exists():
            save_dir.mkdir()

        evaluate_performance_metrics(y_true=cat_gt_labels, y_pred=cat_pr_labels,
                                     output_dir=str(save_dir),  normalize_confusion_matrix=False)
        metrics_df = pd.read_csv(str(save_dir / 'csv_metrics.csv'))
        confmat_str = metrics_df['Confusion Matrix'][0]
        confmat = np.array(np.mat(confmat_str.replace('[', '').replace(']', '').replace('\n ', ';')))

        plot_confmat(confmat_dir=save_dir, cm=confmat)

def main():
    if not METRICS_DIR.exists():
        METRICS_DIR.mkdir()

    skin_tone_dict = separate_by_ita(SKIN_TONE_CSV, TEST_CSV)

    test_df = pd.read_csv(str(TEST_CSV))
    # val_df = pd.read_csv(str(VAL_CSV))
    # skin_tone_df = pd.concat([test_df, val_df]).reset_index()
    im_paths, gt_labels = test_df['image'], test_df['label']

    evaluate_on_categories(im_paths, gt_labels, skin_tone_dict, METRICS_DIR)

    labels = ['very_lt', 'lt2', 'lt1', 'int2', 'int1', 'tan2', 'tan1', 'dark']
    metrics = {'Accuracy':[], 'F1 Score':[], 'Weighted Kappa':[]}
    cis = []
    for label in labels.copy():
        dir_list = [p for p in METRICS_DIR.glob(label) if p.is_dir()]
        if len(dir_list) == 0:
            labels.remove(label)
            continue
        else:
            dir = dir_list[0]

        metrics_df = pd.read_csv(str(dir / 'csv_metrics.csv'))

        acc_w_ci, f1, kappa = metrics_df["Accuracy"][0].split(' '), metrics_df["F1 Score"][0], metrics_df["Weighted Kappa"][0]

        metrics['Accuracy'].append(float(acc_w_ci[0]) / 100)
        cis.append(float(acc_w_ci[1].replace('(','').replace(')','')) / 100)

        metrics['F1 Score'].append(float(f1))

        metrics['Weighted Kappa'].append((float(kappa)))

    for metric in metrics.keys():
        plt.axes().yaxis.grid(zorder=0)
        plt.bar(list(range(len(metrics[metric]))), metrics[metric],
                yerr=cis if metric == 'Accuracy' else None, tick_label=labels, capsize=5, zorder=3)
        plt.yticks(np.linspace(0, 1, num=6))
        plt.title(metric)
        plt.savefig(str(METRICS_DIR / f'{metric.lower().replace(" ", "_")}_plot.png'))
        plt.close()


if __name__ == '__main__':
    main()